/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin.bundle;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.gradle.internal.impldep.org.junit.Rule;
import org.gradle.internal.impldep.org.junit.rules.TemporaryFolder;
import org.gradle.testkit.runner.BuildResult;
import org.gradle.testkit.runner.GradleRunner;
import org.gradle.util.GFileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class LibJavaWorkingProjectTest {
    private GradleRunner project;
    @Rule
    private TemporaryFolder testProjectDir = new TemporaryFolder();
    private File tmp;
    private Git git;

    @BeforeEach
    void setup() throws Exception {
        testProjectDir.create();
        File working = Paths.get( "tp/lib/works/java" ).toFile();
        tmp = testProjectDir.newFolder();

        git = new InitCommand().setDirectory( tmp ).call();

        GFileUtils.copyDirectory( working, tmp );
    }

    @Test
    void tagged() throws GitAPIException {
        RevCommit one = git.commit().setMessage( "initial" ).setAllowEmpty( true ).call();
        git.tag().setAnnotated( true ).setMessage( "initial" ).setName( "v0.1.1" ).setObjectId( one ).call();

        project = GradleRunner.create().withProjectDir( tmp ).withPluginClasspath();

        BuildResult build = project.withArguments( "build", "--stacktrace", "--info" ).build();
        String output = build.getOutput();
        LoggerFactory.getLogger( this.getClass() ).info( build::getOutput );
        assertThat( output ).contains( "BUILD SUCCESSFUL" );

        Path jar = project.getProjectDir().toPath().resolve( "build/libs/working-0.1.1.jar" );

        assertThat( jar ).exists();
    }

    @Test
    void untagged() throws GitAPIException {
        RevCommit one = git.commit().setMessage( "initial" ).setAllowEmpty( true ).call();

        project = GradleRunner.create().withProjectDir( tmp ).withPluginClasspath();

        BuildResult build = project.withArguments( "build", "--stacktrace", "--info" ).build();
        String output = build.getOutput();
        LoggerFactory.getLogger( this.getClass() ).info( build::getOutput );
        assertThat( output ).contains( "BUILD SUCCESSFUL" );

        Path jar = project.getProjectDir().toPath().resolve( "build/libs/working.jar" );

        assertThat( jar ).exists();
    }

    @Test
    void ongoing() throws GitAPIException, IOException {
        RevCommit one = git.commit().setMessage( "initial" ).setAllowEmpty( true ).call();
        git.tag().setAnnotated( true ).setMessage( "initial" ).setName( "v0.1.1" ).setObjectId( one ).call();
        RevCommit a = git.commit().setMessage( "a" ).setAllowEmpty( true ).call();
        RevCommit b = git.commit().setMessage( "b" ).setAllowEmpty( true ).call();
        RevCommit c = git.commit().setMessage( "c" ).setAllowEmpty( true ).call();
        RevCommit d = git.commit().setMessage( "d" ).setAllowEmpty( true ).call();
        git.tag().setAnnotated( true ).setMessage( "initial" ).setName( "v0.1.2" ).setObjectId( c ).call();


        project = GradleRunner.create().withProjectDir( tmp ).withPluginClasspath();

        BuildResult build = project.withArguments( "build", "--stacktrace", "--info" ).build();
        String output = build.getOutput();
        LoggerFactory.getLogger( this.getClass() ).info( build::getOutput );
        assertThat( output ).contains( "BUILD SUCCESSFUL" );

        Path libs = project.getProjectDir().toPath().resolve( "build/libs" );
        try ( Stream<Path> jars = Files.find(
                libs,
                1,
                ( path, basicFileAttributes ) -> path.getFileName().toFile().getName().matches( "working.*\\.jar" )
        ) ) {
            assertThat( jars ).hasSize( 1 );
        }
    }

}
