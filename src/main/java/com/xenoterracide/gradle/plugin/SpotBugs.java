/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import com.github.spotbugs.SpotBugsExtension;
import com.github.spotbugs.SpotBugsPlugin;
import com.github.spotbugs.SpotBugsReports;
import com.github.spotbugs.SpotBugsTask;
import com.xenoterracide.gradle.plugin.bundle.XenoExtension;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.ExtensionContainer;

public class SpotBugs implements Plugin<Project> {

    @Override
    public void apply( Project project ) {
        project.getPluginManager().apply( SpotBugsPlugin.class );
        ExtensionContainer extensions = project.getExtensions();
        extensions.configure( SpotBugsExtension.class, ext -> {
            ext.setToolVersion( "3.1.8" );
            ext.setEffort( "max" );
            ext.setReportLevel( "low" );
            ext.setSourceSets( extensions.getByType( XenoExtension.class ).getSourceSets() );
            ext.setIgnoreFailures( false );
        } );
        project.getTasks().withType( SpotBugsTask.class ).configureEach( task -> {
            task.setEnabled( true );
            SpotBugsReports reports = task.getReports();
            reports.getXml().setEnabled( false );
        } );
    }
}
