/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import com.xenoterracide.gradle.allignmentrules.ImmutablesAllignmentRule;
import com.xenoterracide.gradle.allignmentrules.KotlinAllignmentRule;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static com.xenoterracide.gradle.plugin.DepConstants.APT;
import static com.xenoterracide.gradle.plugin.DepConstants.COMPILE;
import static com.xenoterracide.gradle.plugin.DepConstants.IMPL;
import static com.xenoterracide.gradle.plugin.DepConstants.LATEST;
import static com.xenoterracide.gradle.plugin.DepConstants.TEST_COMPILE;
import static com.xenoterracide.gradle.plugin.DepConstants.TEST_IMPL;
import static com.xenoterracide.gradle.plugin.Dependencies.D;
import static com.xenoterracide.gradle.plugin.Dependencies.EqualsVerifier;

public class DependencyManagement implements Plugin<Project> {
    private final Logger log = LoggerFactory.getLogger( this.getClass() );
    private final String platform = "org.springframework.boot:spring-boot-starter-parent:2.1.+";
    private final List<String> scopes = Arrays.asList( APT, COMPILE, IMPL, TEST_COMPILE );


    private static String immutables( String artifact ) {
        return String.join( D, "org.immutables", artifact, "2.+" );
    }

    @Override
    public void apply( Project project ) {
        log.info( "starting plugin {} for: {}", this.getClass().getSimpleName(), project.getName() );

        project.getConfigurations().all( conf -> {
            conf.resolutionStrategy( rs -> rs.cacheChangingModulesFor( 1, TimeUnit.MINUTES ) );
        } );

        DependencyHandler deps = project.getDependencies();
        deps.components( cmh -> cmh.all( ImmutablesAllignmentRule.class ) );
        deps.components( cmh -> cmh.all( KotlinAllignmentRule.class ) );
        scopes.forEach( scope -> deps.add( scope, deps.enforcedPlatform( platform ) ) );
        deps.constraints( dch -> {
            scopes.forEach( scope -> {
                Stream.of( "value", "builder" ).forEach( artifact -> dch.add( scope, immutables( artifact ) ) );
            } );
            dch.add( TEST_IMPL, String.join( D, EqualsVerifier.G, EqualsVerifier.A, EqualsVerifier.V ) );
            dch.add( IMPL, "com.google.guava:guava:" + LATEST );
        } );
    }
}
