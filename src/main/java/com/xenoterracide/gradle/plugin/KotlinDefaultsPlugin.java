/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.jetbrains.kotlin.gradle.plugin.KotlinPluginWrapper;
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile;

import java.util.Collections;

public class KotlinDefaultsPlugin implements Plugin<Project> {
    @Override
    public void apply( Project project ) {
        project.getPluginManager().apply( KotlinPluginWrapper.class );
        project.getTasks().withType( KotlinCompile.class, task -> {
            task.getKotlinOptions().setJvmTarget( "1.8" );
            task.getKotlinOptions().setFreeCompilerArgs( Collections.singletonList( "-Xjsr305=strict" ) );
        } );
        DependencyHandler deps = project.getDependencies();
        deps.add( DepConstants.IMPL, "org.jetbrains.kotlin:kotlin-stdlib-jdk8" );
        deps.add( DepConstants.IMPL, "org.jetbrains.kotlin:kotlin-reflect" );
    }
}
