/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import com.xenoterracide.gradle.plugin.Dependencies.AssertJ;
import com.xenoterracide.gradle.plugin.Dependencies.Junit;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.plugins.JavaLibraryPlugin;
import org.gradle.api.tasks.testing.Test;
import org.gradle.api.tasks.testing.logging.TestLogEvent;

import static com.xenoterracide.gradle.plugin.DepConstants.TEST_IMPL;
import static com.xenoterracide.gradle.plugin.DepConstants.TEST_RUN;
import static com.xenoterracide.gradle.plugin.Dependencies.D;

public class TestSuiteConfigPlugin implements Plugin<Project> {
    @Override
    public void apply( Project project ) {
        project.getPluginManager().apply( JavaLibraryPlugin.class );
        DependencyHandler deps = project.getDependencies();
        deps.add( TEST_IMPL, String.join( D, Junit.G, Junit.Api.A ) );
        deps.add( TEST_IMPL, String.join( D, Junit.G, Junit.Param.A ) );
        deps.add( TEST_RUN, String.join( D, Junit.G, Junit.Engine.A ) );
        deps.add( TEST_IMPL, String.join( D, AssertJ.G, AssertJ.A ) );

        project.getTasks().withType( Test.class ).configureEach( task -> {
            task.useJUnitPlatform();
            task.testLogging( log -> log.info( info -> info.getEvents().add( TestLogEvent.PASSED ) ) );
            task.reports( reports -> {
                reports.getJunitXml().setEnabled( true );
                reports.getHtml().setEnabled( false );
            } );
        } );
    }
}
