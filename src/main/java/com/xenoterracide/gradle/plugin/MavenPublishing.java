/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;


import com.xenoterracide.gradle.plugin.bundle.XenoExtension;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.credentials.AwsCredentials;
import org.gradle.api.publish.PublishingExtension;
import org.gradle.api.publish.maven.MavenPom;
import org.gradle.api.publish.maven.MavenPublication;
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.TaskProvider;
import org.gradle.jvm.tasks.Jar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

public class MavenPublishing implements Plugin<Project> {

    private final Logger log = LoggerFactory.getLogger( this.getClass() );
    private static final String ENV = "JRS_S3_URI";

    @Override
    public void apply( Project project ) {
        String s3 = System.getenv( ENV );
        log.info( ENV + "={}", s3 );
        if ( s3 != null ) {
            project.getPluginManager().apply( MavenPublishPlugin.class );
            TaskContainer tasks = project.getTasks();
            project.getExtensions().configure( PublishingExtension.class, ext -> {
                ext.repositories( repos -> {
                    repos.maven( repo -> {
                        repo.setUrl( URI.create( s3 ) );
                        repo.credentials( AwsCredentials.class, creds -> {
                            creds.setAccessKey( System.getenv( "JRS_ACCESSKEYID" ) );
                            creds.setSecretKey( System.getenv( "JRS_SECRETACCESSKEY" ) );
                        } );
                    } );
                } );
                ext.publications( pubs -> {
                    TaskProvider<Jar> provider = tasks.register( "sources", Jar.class, jar -> {
                        jar.setClassifier( jar.getName() );
                        project.getExtensions()
                               .getByType( XenoExtension.class )
                               .getSourceSets()
                               .stream()
                               .map( SourceSet::getAllSource )
                               .forEach( jar::from );


                    } );
                    pubs.create( "mavenJava", MavenPublication.class, pub -> {
                        MavenPom pom = pub.getPom();
                        pom.developers( devs -> {
                            devs.developer( dev -> {
                                dev.getName().set( "Caleb Cushing" );
                                dev.getEmail().set( "xenoterracide@gmail.com" );
                                dev.getUrl().set( "https://xenoterracide.com" );
                            } );
                        } );
                        pub.from( project.getComponents().getAt( "java" ) );
                        pub.artifact( provider.get() );
                    } );
                } );
            } );
        }
    }
}
