/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.tasks.compile.CompileOptions;
import org.gradle.api.tasks.compile.JavaCompile;

import java.nio.file.Paths;
import java.util.Arrays;


public class JavaDefaultsPlugin implements Plugin<Project> {

    @Override
    public void apply( Project project ) {
        project.getTasks().withType( JavaCompile.class ).configureEach( task -> {
            CompileOptions options = task.getOptions();
            options.setAnnotationProcessorGeneratedSourcesDirectory( Paths.get( "build/generated/src" ).toFile() );
            options.getCompilerArgs().addAll( Arrays.asList(
                    "-parameters",
                    "-Xlint:deprecation"
            ) );
        } );
    }
}
