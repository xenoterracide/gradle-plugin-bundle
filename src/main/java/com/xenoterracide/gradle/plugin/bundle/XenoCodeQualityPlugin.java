/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin.bundle;

import com.github.spotbugs.SpotBugsExtension;
import com.xenoterracide.gradle.plugin.CheckStyle;
import com.xenoterracide.gradle.plugin.ErrorProneDefaultsPlugin;
import com.xenoterracide.gradle.plugin.IntelliJDefaultsPlugin;
import com.xenoterracide.gradle.plugin.SpotBugs;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.plugins.quality.CheckstyleExtension;
import org.gradle.api.tasks.SourceSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


public class XenoCodeQualityPlugin implements Plugin<Project> {

    private final Logger log = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void apply( Project project ) {
        log.info( "starting plugin {} for: {}", this.getClass().getSimpleName(), project.getName() );
        ExtensionContainer extensions = project.getExtensions();
        extensions.create( "xeno", XenoExtension.class );

        plugins().forEach( project.getPluginManager()::apply );

        extensions.configure( XenoExtension.class, xeno -> {
            if ( xeno.getSourceSets().isEmpty() ) {
                JavaPluginConvention plugin = project.getConvention().getPlugin( JavaPluginConvention.class );
                SourceSet main = plugin.getSourceSets().findByName( SourceSet.MAIN_SOURCE_SET_NAME );
                xeno.setSourceSets( Collections.singleton( main ) );
            }
            extensions.getByType( CheckstyleExtension.class ).setSourceSets( xeno.getSourceSets() );
            extensions.getByType( SpotBugsExtension.class ).setSourceSets( xeno.getSourceSets() );
        } );
    }

    Collection<Class<? extends Plugin<?>>> plugins() {
        return Arrays.asList(
                IntelliJDefaultsPlugin.class,
                CheckStyle.class,
                ErrorProneDefaultsPlugin.class,
                SpotBugs.class
        );
    }
}
