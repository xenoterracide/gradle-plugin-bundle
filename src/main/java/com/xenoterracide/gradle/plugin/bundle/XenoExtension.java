/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin.bundle;

import org.gradle.api.tasks.SourceSet;

import java.util.Collection;
import java.util.Collections;

public class XenoExtension {

    private Collection<SourceSet> sourceSets = Collections.emptySet();
    private boolean javaLib = true;

    public Collection<SourceSet> getSourceSets() {
        return sourceSets;
    }

    public void setSourceSets( Collection<SourceSet> sourceSets ) {
        this.sourceSets = sourceSets;
    }

    public boolean isJavaLib() {
        return javaLib;
    }

    public void setJavaLib( boolean javaLib ) {
        this.javaLib = javaLib;
    }
}
