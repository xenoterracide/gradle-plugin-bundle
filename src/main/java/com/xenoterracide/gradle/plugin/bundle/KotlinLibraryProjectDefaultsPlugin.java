/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin.bundle;

import com.xenoterracide.gradle.plugin.DependencyManagement;
import com.xenoterracide.gradle.plugin.IntelliJDefaultsPlugin;
import com.xenoterracide.gradle.plugin.KotlinDefaultsPlugin;
import com.xenoterracide.gradle.plugin.MavenPublishing;
import com.xenoterracide.gradle.plugin.MavenRepositories;
import com.xenoterracide.gradle.plugin.SemVerPlugin;
import com.xenoterracide.gradle.plugin.TestSuiteConfigPlugin;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.SourceSetContainer;

import java.util.Arrays;
import java.util.Collections;

public class KotlinLibraryProjectDefaultsPlugin implements Plugin<Project> {
    @Override
    public void apply( Project project ) {
        ExtensionContainer extensions = project.getExtensions();
        extensions.create( "xeno", XenoExtension.class );

        extensions.configure( XenoExtension.class, xeno -> {
            if ( xeno.getSourceSets().isEmpty() ) {
                project.getPluginManager().withPlugin( "kotlin-source-set", p -> {
                    SourceSet main =
                            project.getExtensions()
                                   .getByType( SourceSetContainer.class )
                                   .getByName( SourceSet.MAIN_SOURCE_SET_NAME );
                    xeno.setSourceSets( Collections.singleton( main ) );
                } );
            }
        } );
        Arrays.asList(
                IntelliJDefaultsPlugin.class,
                TestSuiteConfigPlugin.class,
                KotlinDefaultsPlugin.class,

                DependencyManagement.class,
                SemVerPlugin.class,
                MavenRepositories.class,
                MavenPublishing.class
        ).forEach( plugin -> project.getPluginManager().apply( plugin ) );


    }
}
