/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xenoterracide.gradle.plugin;

import net.ltgt.gradle.errorprone.CheckSeverity;
import net.ltgt.gradle.errorprone.ErrorProneOptions;
import net.ltgt.gradle.errorprone.ErrorPronePlugin;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.plugins.ExtensionAware;
import org.gradle.api.tasks.compile.CompileOptions;
import org.gradle.api.tasks.compile.JavaCompile;

import java.nio.file.Paths;

import static com.xenoterracide.gradle.plugin.DepConstants.COMPILE;
import static com.xenoterracide.gradle.plugin.DepConstants.EPV;

public class ErrorProneDefaultsPlugin implements Plugin<Project> {

    @Override
    public void apply( Project project ) {
        project.getPluginManager().apply( ErrorPronePlugin.class );

        project.getTasks().withType( JavaCompile.class ).whenTaskAdded( javaCompile -> {
            DependencyHandler deps = project.getDependencies();
            deps.add( "errorprone", "com.google.errorprone:error_prone_core:" + EPV );
            deps.add( COMPILE, "com.google.errorprone:error_prone_annotations:" + EPV );
        } );

        project.getTasks().withType( JavaCompile.class ).configureEach( task -> {
            CompileOptions options = task.getOptions();
            options.setAnnotationProcessorGeneratedSourcesDirectory( Paths.get( "build/generated/src" ).toFile() );
            ErrorProneOptions ep = ( (ExtensionAware) options ).getExtensions().getByType( ErrorProneOptions.class );
            ep.check( "MissingOverride", CheckSeverity.ERROR );
            ep.check( "Var", CheckSeverity.ERROR );
            ep.setExcludedPaths( ".*/build/generated/.*" );
        } );

    }
}
