/*
 * Copyright 2018 Caleb Cushing
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.github.spotbugs.SpotBugsTask
import com.xenoterracide.gradle.plugin.configurations.ErrorProne
import com.xenoterracide.gradle.plugin.configurations.SpotBugs
import net.ltgt.gradle.errorprone.CheckSeverity
import net.ltgt.gradle.errorprone.errorprone
import org.gradle.api.credentials.AwsCredentials
import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.gradle.internal.impldep.org.bouncycastle.asn1.x500.style.RFC4519Style.c
import org.gradle.kotlin.dsl.maven
import org.gradle.kotlin.dsl.repositories
import org.gradle.kotlin.dsl.support.illegalElementType
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.gradle.plugins.ide.idea.model.IdeaProject

group = "com.xenoterracide.gradle"

plugins {
    idea
    checkstyle
    `java-gradle-plugin`
    id("net.ltgt.errorprone").version("0.6")
    id("com.github.spotbugs").version("1.6.3")
    id("com.xenoterracide.gradle.sem-ver").version("0.5.2")
    id("com.gradle.plugin-publish").version("0.10.0")
}

repositories {
    maven("https://plugins.gradle.org/m2/")
    mavenCentral()
}

dependencies {
    implementation("org.apache.commons:commons-lang3:3.+")
    errorprone("com.google.errorprone:error_prone_core:2.+")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.2.+")
    implementation("gradle.plugin.com.github.spotbugs:spotbugs-gradle-plugin:1.6.4")
    implementation("net.ltgt.gradle:gradle-errorprone-plugin:0.6")
    implementation("org.eclipse.jgit:org.eclipse.jgit:5.+")
    implementation("org.apache.commons:commons-lang3:3.+")
    // implementation("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.+")


    testRuntimeOnly(gradleKotlinDsl())
    testImplementation(platform("org.junit:junit-bom:5.+"))

    testImplementation("org.assertj:assertj-core:3.+")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

dependencyLocking {
    lockAllConfigurations()
}

spotbugs {
    toolVersion = "3.1.8"
    effort = "max"
    reportLevel = "low"
    sourceSets = setOf(project.sourceSets["main"])
}

tasks.withType<SpotBugsTask>().configureEach {
    excludeFilter = file("config/spotbugs/exclude.xml")
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
    testLogging {
        showExceptions = true
        showCauses = true
        showStandardStreams = true
    }
    reports {
        junitXml.isEnabled = true
        html.isEnabled = false
    }
}

checkstyle {
    isShowViolations = true
    sourceSets = setOf(project.sourceSets["main"])
}

tasks.withType<Checkstyle>().configureEach {
    reports {
        xml.isEnabled = false
        html.isEnabled = false
    }
}

tasks.withType<JavaCompile>().configureEach {
    sourceCompatibility = "8"
    targetCompatibility = "8"

    options.isFork = false
    val ep = options.errorprone
    ep.errorproneArgs.addAll(listOf("-Xmx2g"))
    // ep.check("Var", CheckSeverity.ERROR)
    ep.check("MissingOverride", CheckSeverity.ERROR)
}

pluginBundle {
    // These settings are set for the whole plugin bundle
    vcsUrl = "https://bitbucket.org/xenoterracide/gradle-plugin-bundle"
    website = vcsUrl
    plugins {

        create("error-prone-defaults") {
            id = "com.xenoterracide.gradle.$name"
            displayName = "Xeno's Default Error Prone Settings"
            description = displayName
            tags = listOf("defaults")
        }
        create("java-defaults") {
            id = "com.xenoterracide.gradle.$name"
            displayName = "Xeno's Default Java Compiler Settings"
            description = displayName
            tags = listOf("defaults")
        }
        create("sem-ver") {
            id = "com.xenoterracide.gradle.$name"
            displayName = "Xeno's SemVer with Git"
            description = "Use pure java to parse semantic git tags, and add a project.version"
            tags = listOf("git")
        }

        create("kotlin-lib-defaults") {
            id = "com.xenoterracide.gradle.bundle.$name"
            displayName = "Xeno's Defaults for a Kotlin Library"
            description = displayName
            tags = listOf("defaults")
        }
        create("java-project-defaults") {
            id = "com.xenoterracide.gradle.bundle.$name"
            displayName = "Xeno's defaults for Java Libaries"
            description = "Xeno's bundle for building Java Libraries, includes Code Quality"
            tags = listOf("authorbundle")
        }
        create("code-quality") {
            id = "com.xenoterracide.gradle.bundle.$name"
            displayName = "Xeno's Code Quality configuration"
            description = "Xeno's bundle for Code Quality"
            tags = listOf("authorbundle")
        }
    }
}
